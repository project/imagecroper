# Image Crop Widget

Offering user have option to edit images in image form: rotate, crop.

## Table of contents

- Requirements
- Installation
- Configuration
- Uninstallation
- UI Information
- Maintainers

## Requirements

The basic imager_widget module has media dependencies.

## Installation

1. Install as you would normally install a contributed Drupal module.
     See: <https://www.drupal.org/node/895232/> for further information.

## Configuration

1. Install module “Image Crop Widget”.
1. Go to the “Manage form display”.
1. Eg:- Admin >> structure >> Content types >> Manage form display
1. Then you just need to choose imager widget option for image.
1. When you have upload any image then you can see a button start editing".
1. Then you need click on that button.
1. You can see two option there one crop and rotate
1. Then you can changes as per your requirement and click on save button.

## Uninstallation

Uninstall as you would normally Uninstall a contributed Drupal module.
See: <https://www.drupal.org/docs/user_guide/en/config-uninstall.html/>
for further information.

## UI Information

1. For the UI, the Developer will use it time of add/edit node

## Maintainers

- Shashank Kumar - [shashank5563](https://www.drupal.org/u/shashank5563)
